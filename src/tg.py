import bot
import falcon
import os
import requests

TG_API_URL = f"https://api.telegram.org/bot{os.environ['TG_BOT_TOKEN']}/"


class Telegram():
    def call_api(self, method, params={}):
        r = requests.post(TG_API_URL + method, params=params)
        if not r:
            raise falcon.HTTPBadGateway

        result = r.json()
        if 'error' in result:
            raise falcon.HTTPBadGateway
        return result

    def escape_md(self, text):
        return (text
            .replace('_', r'\_')
            .replace('*', r'\*')
            .replace('[', r'\[')
            .replace(']', r'\]')
            .replace('(', r'\(')
            .replace(')', r'\)')
            .replace('~', r'\~')
            .replace('>', r'\>')
            .replace('#', r'\#')
            .replace('+', r'\+')
            .replace('-', r'\-')
            .replace('=', r'\=')
            .replace('|', r'\|')
            .replace('{', r'\{')
            .replace('}', r'\}')
            .replace('.', r'\.')
            .replace('!', r'\!')
        )

    def parse_message(self, event):
        chat_id = (event.get('message')
            and event['message'].get('chat')
            and event['message']['chat'].get('id'))
        text = (event.get('message')
            and event['message'].get('text'))
        return chat_id, text

    def send_message(self, chat_id, text):
        self.call_api('sendMessage', {
            'parse_mode': 'MarkdownV2',
            'chat_id': chat_id,
            'text': self.escape_md(text),
        })

    def on_post(self, req, resp):
        chat_id, text = self.parse_message(req.media)
        bot.LenochkaBot(self).handle_message(chat_id, text)
