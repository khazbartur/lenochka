import database
import falcon
import os
import tg

TG_WEBHOOK_SECRET = os.environ['TG_WEBHOOK_SECRET']
TG_TME_URL = os.environ['TG_TME_URL']


class Home:
    def on_get(self, req, resp):
        raise falcon.HTTPSeeOther(TG_TME_URL)


app = falcon.App(middleware=[database.PeeweeMiddleware()])
app.add_route('/', Home())
app.add_route('/tg/' + TG_WEBHOOK_SECRET, tg.Telegram())
