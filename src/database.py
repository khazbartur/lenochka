from playhouse import db_url

import os
import peewee

database = db_url.connect(os.environ['DB_URL'])


class Entity(peewee.Model):
	class Meta:
		database = database


class PeeweeMiddleware:
	def process_resource(self, req, resp, resource, params):
		database.connect()

	def process_response(self, req, resp, resource, req_succeeded):
		if not database.is_closed():
			database.close()
