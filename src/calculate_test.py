from decimal import Decimal
import calculate


def test_1():
    fv = Decimal('1250')
    pv = Decimal('1000')
    r  = Decimal('0.05')
    n  = Decimal('5')

    assert fv == calculate.fv__pv_r_n(pv, r, n)
    assert pv == calculate.pv__fv_r_n(fv, r, n)
    assert r  == calculate.r__fv_pv_n(fv, pv, n)
    assert n  == calculate.n__fv_pv_r(fv, pv, r)


def test_2():
    fv = Decimal('1250')
    pv = Decimal('1000')
    r  = Decimal('0.05')
    t  = Decimal('1825')

    assert fv == calculate.fv__pv_r_t(pv, r, t)
    assert pv == calculate.pv__fv_r_t(fv, r, t)
    assert r  == calculate.r__fv_pv_t(fv, pv, t)
    assert t  == calculate.t__fv_pv_r(fv, pv, r)



if __name__ == '__main__':
    test_1()
    test_2()
