import calculate
import decimal
import enum
import inspect
import json
import operator as op
import random
import re


class TokenType(enum.Enum):
    ANY = 0
    VALUE = 1
    VARIABLE = 2


token_regex = re.compile('<(.*?)>')
decimal_regex = re.compile(r'-?(\d+\.\d+|\d+|\d*\.\d+)')
with open('src/tokens.json', 'r') as token_file:
    token_map = json.load(token_file)


class LenochkaBot:
    def __init__(self, medium):
        self.medium = medium

    def normalize_text(self, text):
        return text.lower().replace('ё', 'е')

    def tokenize_text(self, text):
        tok_text = self.normalize_text(text)
        for phrase, token in token_map.items():
            tok_text = tok_text.replace(phrase, token)
        tok_text = decimal_regex.sub(r'<\g<0>>', tok_text)
        return token_regex.findall(tok_text)

    def parse_task(self, tokens):
        expected = TokenType.ANY
        variables = {}
        qm = False
        for token in tokens:
            if token not in (':', ';') and not (qm and token == '?'):
                if expected == TokenType.ANY:
                    if token == '?':
                        qm = True
                        value = '?'
                        expected = TokenType.VARIABLE
                    elif token.isalpha():
                        variable = token
                        expected = TokenType.VALUE
                    else:
                        value = decimal.Decimal(token)
                        expected = TokenType.VARIABLE
                elif expected == TokenType.VALUE:
                    if token == '?':
                        qm = True
                        variables[variable] = '?'
                    else:
                        variables[variable] = decimal.Decimal(token)
                    expected = TokenType.ANY
                elif expected == TokenType.VARIABLE:
                    if token.isalpha():
                        variables[token] = value
                    else:
                        raise ValueError
                    expected = TokenType.ANY
        return variables

    def parse_doc(self, func):
        solution = ''
        lines = inspect.getdoc(func).split('\n')
        for line in lines[1:]:
            if line:
                solution += line[line.rfind(':') + 1:].strip()
            solution += '\n'
        return solution

    def find_solution(self, vars):
        # 5
        if 'rr' in vars or 'a' in vars:
            r, rr, a = op.itemgetter('r', 'rr', 'a')(vars)
            if r == '?':
                solve = calculate.r__rr_a
                return self.parse_doc(solve), solve(rr, a)
            if rr == '?':
                solve = calculate.rr__r_a
                return self.parse_doc(solve), solve(r, a)
            if a == '?':
                solve = calculate.a__r_rr
                return self.parse_doc(solve), solve(r, rr)
        # 4
        if 'm' in vars:
            cr, fv, pv, m, n = op.itemgetter('cr', 'fv', 'pv', 'm', 'n')(vars)
            if fv == '?':
                solve = calculate.fv__pv_cr_m_n
                return self.parse_doc(solve), solve(pv, cr, m, n)
            if pv == '?':
                solve = calculate.pv__fv_cr_m_n
                return self.parse_doc(solve), solve(fv, cr, m, n)
            if cr == '?':
                solve = calculate.cr__fv_pv_m_n
                return self.parse_doc(solve), solve(fv, pv, m, n)
            if m == '?':
                solve = calculate.m__fv_pv_cr_n
                return self.parse_doc(solve), solve(fv, pv, cr, n)
            if n == '?':
                solve = calculate.n__fv_pv_cr_m
                return self.parse_doc(solve), solve(fv, pv, cr, m)
        # 3
        if 't' in vars and 'cr' in vars:
            fv, pv, cr, t = op.itemgetter('fv', 'pv', 'cr', 't')(vars)
            if fv == '?':
                solve = calculate.fv__pv_cr_t
                return self.parse_doc(solve), solve(pv, cr, t)
            if pv == '?':
                solve = calculate.pv__fv_cr_t
                return self.parse_doc(solve), solve(fv, cr, t)
            if cr == '?':
                solve = calculate.cr__fv_pv_t
                return self.parse_doc(solve), solve(fv, pv, t)
            if t == '?':
                solve = calculate.t__fv_pv_cr
                return self.parse_doc(solve), solve(fv, pv, cr)
        # 2
        if 't' in vars:
            fv, pv, r, t = op.itemgetter('fv', 'pv', 'r', 't')(vars)
            if fv == '?':
                solve = calculate.fv__pv_r_t
                return self.parse_doc(solve), solve(pv, r, t)
            if pv == '?':
                solve = calculate.pv__fv_r_t
                return self.parse_doc(solve), solve(fv, r, t)
            if r == '?':
                solve = calculate.r__fv_pv_t
                return self.parse_doc(solve), solve(fv, pv, t)
            if t == '?':
                solve = calculate.t__fv_pv_r
                return self.parse_doc(solve), solve(fv, pv, r)
        # 1
        if 'n' in vars:
            fv, pv, r, n = op.itemgetter('fv', 'pv', 'r', 'n')(vars)
            if fv == '?':
                solve = calculate.fv__pv_r_n
                return self.parse_doc(solve), solve(pv, r, n)
            if pv == '?':
                solve = calculate.pv__fv_r_n
                return self.parse_doc(solve), solve(fv, r, n)
            if r == '?':
                solve = calculate.r__fv_pv_n
                return self.parse_doc(solve), solve(fv, pv, n)
            if n == '?':
                solve = calculate.t__fv_pv_n
                return self.parse_doc(solve), solve(fv, pv, n)

        return None, None

    def handle_message(self, chat_id, text):
        if not (chat_id and text):
            return

        greet = False
        if any(w in text.lower() for w in ('/start', 'прив', 'здравств', 'здраст', 'здаров', 'здоров')):
            greet = True
            self.medium.send_message(chat_id, 'Добрый день! 😌')

        if 'кто' in text.lower() and ('автор' in text.lower() or 'созда' in text.lower()):
            self.medium.send_message(chat_id, 'Меня создали @Vlad_Maslennikov и @x_Arthur_x')
            return

        if 'отдебажу' in text.lower():
            self.medium.send_message(chat_id, 'Ну давайте подебажимся 😉')

        tokens = self.tokenize_text(text)
        if 'отдебажу' in text.lower():
            self.medium.send_message(chat_id, ' '.join(tokens) or '...')

        try:
            variables = self.parse_task(tokens)
        except (ValueError, decimal.InvalidOperation):
            self.medium.send_message(chat_id, 'Я не совсем поняла задачу 🥲')
            return

        if 'отдебажу' in text.lower():
            self.medium.send_message(chat_id, json.dumps(variables, default=str))

        if not (variables or greet):
            fallback_text = random.choice([
                'Что? Не поняла...',
                'Не знаю, что ответить',
                'Я не умею такое считать',
                'Я ничегошеньки не поняла',
                'Не понимаю, что вы хотите',
                'Я так никогда не пробовала',
                'Я в этом не разбираюсь, извините',
                'Согласно моей формуле... Хм, у меня нет такой формулы',
                'Не знаю... Похоже, разработчики совсем надо мной не старались',
            ])
            self.medium.send_message(chat_id, f'{fallback_text} 🥲')
            return

        try:
            solution, result = self.find_solution(variables)
        except KeyError:
            self.medium.send_message(chat_id, 'Мне не хватает некоторых данных 🥲')
            return
        if not (solution or greet):
            self.medium.send_message(chat_id, 'Я не знаю, как это решить 🥲')
            return

        if solution:
            success_text = random.choice([
                'Выходит',
                'Получается',
                'Я насчитала',
                'У меня вышло',
                'У меня получилось',
                'Насколько я знаю, будет',
                'Согласно моей формуле, будет',
                'Если верить моей формуле, то это',
                'Если мои разработчики не накосячили, то это',
            ])
            self.medium.send_message(chat_id,
                f'{success_text} {result:.4g} 😇\n'
                f'```\n{solution}```'
            )







